# Readme mongo ip whitelister

## venv, activate, requeriments, run app
```
python3 -m venv env
source env/bin/activate
pip3 install -r requeriments.txt
```
Copy start.sh as run.sh and fill the secrets env var and run with:
```
bash ./run.sh
```

### to deactivate the venv:

```
deactivate
```

## env vars used:


#### GOOGLE_SSO_CLIENT_ID & GOOOGLE_SSO_SECRET
    Values obtained from google cloud console to auth with oauth app.


#### MDBA_CLIENT & MDBA_SECRET 
    Values from mongodbatlas to perform the auth with the api of mongodbatlas.
#### URL_CALLBACK
    URL used by google oauth callback, sould be url of app + /google/callback.

#### LOCAL_ENV
    Set only in local environment to true mode to run the google_sso as http insecure.

#### PROJECT_ID
    json dict with key value of environment:projectid

#### HEADER
    Header to use to obtain Remote-IP.

This env are set in terraform, repo: ph-platform-infra.