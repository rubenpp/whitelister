import requests
from requests.auth import HTTPDigestAuth
import json
import ipaddress
from datetime import datetime, timedelta
from app.config.settings import MDBA_CLIENT, MDBA_SECRET, PROJECT_ID, LOCAL_ENV, HEADER

def add_ip(ip, useremail, environment):
  project_id = PROJECT_ID[environment]
  url = "https://cloud.mongodb.com/api/atlas/v1.0/groups/" + project_id + "/accessList?pretty=true"

  payload = json.dumps([
    {
      "ipAddress": ip,
      "comment": useremail,
      "deleteAfterDate": (datetime.now() + timedelta(hours=12)).isoformat() + "+01:00"
    }
  ])
  
  #log stdout
  print (datetime.now().strftime("%d-%m-%y %H:%M:%S") + " ENV="+environment + " " + payload  )
  
  try:
    r = requests.post (  url,
                        headers={ "Accept": "application/json", "Content-Type": "application/json"},
                        auth=HTTPDigestAuth(MDBA_CLIENT,MDBA_SECRET),
                        data=payload
                      )
    return r.status_code, r.json();
  except requests.exceptions.HTTPError as e:
    return e;

def get_public_ip(request):
  if LOCAL_ENV is None:
    ip = request.headers.get(HEADER)
  else:
    ip = "127.0.0.1"
  return (ip)