import os

MDBA_CLIENT = os.environ.get('MDBA_CLIENT') or None
if MDBA_CLIENT is None:
    raise BaseException('Missing MDBA_CLIENT env var.')

MDBA_SECRET = os.environ.get('MDBA_SECRET') or None
if MDBA_SECRET is None:
    raise BaseException('Missing MDBA_SECRET env var.')

GOOGLE_SSO_CLIENT_ID = os.environ.get('GOOGLE_SSO_CLIENT_ID') or None
if GOOGLE_SSO_CLIENT_ID is None:
    raise BaseException('Missing GOOGLE_SSO_CLIENT_ID env var.')

GOOOGLE_SSO_SECRET = os.environ.get('GOOOGLE_SSO_SECRET') or None
if GOOOGLE_SSO_SECRET is None:
    raise BaseException('Missing GOOOGLE_SSO_SECRET env var.')

URL_CALLBACK = os.environ.get('URL_CALLBACK') or None
if URL_CALLBACK is None:
    raise BaseException('Missing URL_CALLBACK env var.')

LOCAL_ENV = os.environ.get('LOCAL_ENV') or None
if LOCAL_ENV is not None:
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

PROJECT_ID = os.environ.get('PROJECT_ID') or None
if PROJECT_ID is None:
    PROJECT_ID = {"dev":"6140b34b355aae578be3477c","staging":"6140de9601e18345ae50b188","prod":"614ca5dc657c162489ab788c"}

HEADER = os.environ.get('HEADER') or None
if HEADER is None:
    HEADER = 'X-Forwarded-For'
    #raise BaseException('Missing HEADER env var.')