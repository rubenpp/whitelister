from app.config.settings import GOOGLE_SSO_CLIENT_ID, GOOOGLE_SSO_SECRET, URL_CALLBACK, LOCAL_ENV
import app.controller.request as mongoapi
import uvicorn
from fastapi import FastAPI, Response, status, HTTPException, Depends, Form
from fastapi.templating import Jinja2Templates
from fastapi_sso.sso.google import GoogleSSO
from email_split import email_split
from pathlib import Path
from starlette.requests import Request
from starlette.middleware.sessions import SessionMiddleware
from starlette.responses import HTMLResponse, JSONResponse, RedirectResponse, FileResponse


app = FastAPI()
app.add_middleware(SessionMiddleware, secret_key='!secret')

BASE_PATH = Path(__file__).resolve().parent
TEMPLATES = Jinja2Templates(directory=str(BASE_PATH / "../templates"))


if LOCAL_ENV is not None:
    google_sso = GoogleSSO(GOOGLE_SSO_CLIENT_ID, GOOOGLE_SSO_SECRET, URL_CALLBACK, allow_insecure_http=True, use_state=False)
else:
    google_sso = GoogleSSO(GOOGLE_SSO_CLIENT_ID, GOOOGLE_SSO_SECRET, URL_CALLBACK, use_state=False)


@app.get('/favicon.ico', include_in_schema=False)
async def favicon():
    return FileResponse("app/static/favicon.ico")


@app.get("/")
async def root(request: Request):
    """Root GET main page"""
    user = request.session.get('user')
    if user is not None:
        return TEMPLATES.TemplateResponse(
            "whitelist.html",
            {   "request": request, "name": user['display_name'], 
                "email": user['email'], "google_image": user['picture'], 
                "ip": mongoapi.get_public_ip(request)
            },
        )
    return TEMPLATES.TemplateResponse(
        "index.html",
        {"request": request},
    )


@app.get("/google/login", tags=['authentication'])
async def google_login():
    """Generate login url and redirect"""
    return await google_sso.get_login_redirect()


async def get_user_valid(request: Request, user: str = Depends(google_login)):
    user = request.session.get('user')
    if ( (user is not None) and ( email_split(user['email']).domain == "europcar.com") ):
        return {'message': 'protected api_app endpoint'}
    else:
        raise HTTPException(status_code=403, detail='Could not validate credentials.')


@app.get('/logout', tags=['authentication'])  # Tag it as "authentication" for our docs
async def logout(request: Request):
    # Remove the user
    request.session.pop('user', None)
    return RedirectResponse(url='/')


@app.get("/google/callback", tags=['authentication'])
async def google_callback(request: Request, response: Response):
    """Process login response from Google and return user info"""
    user = await google_sso.verify_and_process(request)
    # Save the user
    request.session['user'] = dict(user)
    return RedirectResponse(url='/')


@app.post("/whitelist")
async def do_whitelist(request: Request, response: Response , environment: str = Form(...), user: str = Depends(get_user_valid), status_code=status.HTTP_201_CREATED):
    user = request.session.get('user')
    try:
        ip = mongoapi.get_public_ip(request)
        email = user['email']
        return_response = mongoapi.add_ip(ip, email, environment)
        if return_response[0] != 201:
            response.status_code = status.HTTP_400_BAD_REQUEST
            return {"response": return_response[1]}
        return {"response": return_response}
    except Exception as e: 
        message = str(e)
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": message}


if __name__ == '__main__':
    uvicorn.run(app, port=8000)