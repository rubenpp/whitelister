.PHONY: clean python-packages install run all

clean:
	find . -type f -name '*.pyc' -delete
	find . -type f -name '*.log' -delete
	find . -type d -name __pycache__ -delete

python-packages:
	pip install --no-cache-dir --upgrade pip
	pip install --use-feature=2020-resolver --no-cache-dir -r requirements.txt

install: python-packages

run:
	uvicorn app.main:app --reload --host 0.0.0.0 --port 8000

all: clean install run