FROM tiangolo/uvicorn-gunicorn-fastapi:python3.9

#RUN apt update; apt install -y vim

COPY ./ /app
COPY ./requirements.txt /tmp
RUN pip install --no-cache-dir -r /tmp/requirements.txt